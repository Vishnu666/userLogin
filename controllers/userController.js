import User from "../model/userModel.js";
import mongoose from "mongoose";
import bcrypt from "bcrypt";
import generateToken from "../utils/generateToken.js";

//login
export const Login = async (req, res) => {
  const { email, password } = req.body;

  try {
    const existingUser = await User.findOne({ email });
    
    if (!existingUser) {
      return res.json({ message: "User is not found" });
    }
    const isPasswordCorrect = await bcrypt.compare(
      password,
      existingUser.password
    );
    if (!isPasswordCorrect) {
      return res.json({ message: "Incorrect password" });
    }
    
     //token generate
    res.status(200).json({message:"New user successfully LoggedIn",token: await generateToken(existingUser.username)});
  } catch (error) {
    res.status(404).json({ message: "something when wrong!!!" });
  }
};

// Register
export const Register = async (req, res) => {
  console.log(req.file);
  const {
    username,
    firstname,
    lastname,
    age,
    gender,
    address,
    email,
    password,
  } = req.body;

  try {
    const existingUser = await User.findOne({ email });
    if (existingUser) {
      return res.json({ message: "User is already exist" });
    }
    const hashPassword = await bcrypt.hash(password, 10);

    await User.create({
      username,
      firstname,
      lastname,
      age,
      gender,
      address,
      email,
      password: hashPassword,
      image: req.file.filename,
    });

    res.status(200).json({ message: "New User added successfully" });
  } catch (error) {
    res.json(error.message);
  }
};

//get all users
export const usersList = async (req, res) => {
 
  //pagination 
  const page = Number(req.query.page) || 1;
  const limit = 5;
  const startIndex = (page - 1) * limit;

  // username || email
  const username = req.query.username;
  const email = req.query.email;

  // age
  const ageFrom = Number(req.query.ageFrom);
  const ageLimit = Number(req.query.ageLimit);


  try {
     //filtering based on username || email
    if (username || email) {
     let list = await User.aggregate([
        { $match: { $or: [{ username }, { email }] } },
        { $project: { createdAt: 0, updatedAt: 0, __v: 0, password: 0 } },
      ]);
      return res.status(200).json(list);
    }

    //filtering based on age
    if(ageFrom && ageLimit){
      let list =await User.aggregate([
        {$match: {age:{$gte:ageFrom,$lte:ageLimit}}},
        {$sort:{age:1}},
        {$skip:startIndex},
        {$limit: limit},
        { $project: { createdAt: 0, updatedAt: 0, __v: 0, password: 0 } }
       ])
       return res.status(200).json(list)
    }

    //pagination
     let list = await User.aggregate([
        {$sort:{createdAt:-1}},
        {$skip:startIndex},
        {$limit: limit},
        { $project: { createdAt: 0, updatedAt: 0, __v: 0, password: 0 } }
      ])
      return res.status(200).json(list);
  } 
  catch (error) {
    res.json(error.message);
  }
};

//update Users
export const updateUser = async (req, res) => {
  const { firstname, lastname, age, gender, address } = req.body;

  try {
    const isIdValid = mongoose.Types.ObjectId.isValid(req.params.id);

    if (isIdValid) {
      const user = await User.findByIdAndUpdate(
        req.params.id,
        { firstname, lastname, age, gender, address },
        {
          new: true,
        }
      );

      res.json({ user, message: "User updated successfully" });
    }
  } catch (error) {
    res.json(error.message);
  }
};

//delete User
export const deleteUser = async (req, res) => {
  try {
    await User.findByIdAndRemove({ _id: req.params.id });
    res.json({ message: "User deleted successfully" });
  } catch (error) {
    res.json(error.message);
  }
};
