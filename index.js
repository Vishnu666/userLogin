import express from "express";
import dotenv from "dotenv";
import connectdB from "./config/db.js";
import userRoutes from "./routes/userRoutes.js";
import uploadRoutes from "./routes/uploadRoutes.js"
import path from 'path';


const app = express();
dotenv.config();
connectdB();


app.use(express.json());

//user route
app.use("/api/users", userRoutes);

//upload route
// app.use("/api/upload",uploadRoutes)
const __dirname=path.resolve()
app.use("/images",express.static(path.join(__dirname,"/images")))



const PORT = process.env.PORT || 5000;
app.listen(PORT, () => {
  console.log(`server is running on ${PORT}`);
});
