import express from "express";
const router =express.Router()
import multer from "multer"



const storage = multer.diskStorage({
   
    destination: (req, file, cb) => {
      cb(null, "images");
    },
    filename: (req, file, cb) => {
      cb(null, `${req.body.username}.jpg`);
    },
  });
  
 const upload = multer({ storage: storage });



export default upload
