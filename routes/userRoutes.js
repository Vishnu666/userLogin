import express from "express";
import upload from "./uploadRoutes.js";
import protect from "../middleware/authMiddleware.js";
import {
  Login,
  Register,
  deleteUser,
  updateUser,
  usersList,
} from "../controllers/userController.js";

const router = express.Router();

router.get("/",protect, usersList);
router.post("/register",upload.single("image"),Register);
router.post("/login", Login);
router.put("/edit/:id",protect, updateUser);
router.delete("/:id",protect, deleteUser);

export default router;
