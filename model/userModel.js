import mongoose from "mongoose";

const userSchema = new mongoose.Schema({
  username: {
    type: String,
    required: true,
    unique: true,
    minLength: 3,
  },
  firstname: {
    type: String,
    required: true,
    minLength: 4,
  },
  lastname: {
    type: String,
    required: true,
    minLength: 2,
  },
  age: {
    type: Number,
    required: true,
    maxLength: 100,
  },
  gender: {
    type: String,
    required: true,
  },
  address: {
    type: String,
    required: true,
    maxLength: 20,
  },
  email: {
    type: String,
    required: true,
    unique: true,
  },
  password: {
    type: String,
    required: true,
    minLength: 8,
  },
  image: {
    type: String,
    // required: true,
    default:""
   
  },
 },
 {
  timestamps:true
 }
 );

export default mongoose.model("User", userSchema);
